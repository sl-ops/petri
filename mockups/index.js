var express = require('express');
var compass = require('node-compass');
var app = express();

app.use(compass());

app.use(express.static('public'));
//use "npm start" to launch at console.

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
