(function () {
  window.sl = {};

  var satchmoApp = angular.module('satchmoApp', [
    'ngAnimate',
    'ngRoute',
    'satchmoApp.controllers'
    ]);

    //angular.module('d3', []);
    angular.module('satchmoApp.controllers', []);
    //angular.module('satchmoApp.directives', ['d3']);

  satchmoApp.controller('infoCtrl', ['$scope', function($scope, $data){
//**************
//* Initiation *
//**************
    $scope.menuClass = "asideClosed";
    $scope.asideShow = false;

    var tab = '';

//******************
//* Aside Function *
//******************
    $scope.menuView = function() {
      if ($scope.menuClass === "asideClosed") {
        $scope.menuClass = "asideOpened";
        $scope.asideShow = true;
      } else {
        $scope.menuClass = "asideClosed";
        $scope.asideShow = false;
      }
    };

  }]);
})();
